# GellieR - the shinier gel image annotator

GellieR is an R/Shiny App that saves time by automatically annotating electrophoresis gel images with lane numbers, sample names, and band indicators. It currently has support for JPEG and PNG input files.

The App has three outputs that can be chosen:

1. Annotated image with circles
2. Density-adjusted (and annotated) image where bands are extended to the width of each lane
3. CSV table containing information about peaks (e.g. intensity, location)

The application assumes that images have a properly set resolution (e.g. as created by GelDoc/ChemiDoc). If this is not the case, then the gel comb separation will need to be tweaked accordingly.

GellieR can be run locally by loading the code into RStudio and clicking on "Run App". It requires the `imager` R library to be installed for doing image processing.

If you want to try this application out before running locally, I have published this on ShinyApps:

https://gringene.shinyapps.io/gellier/
